SUMMARY = "Ethercat kernel module"
LICENSE = "GPL-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

SRC_URI = "http://www.etherlab.org/download/ethercat/ethercat-${PV}.tar.bz2 \
           file://0001-remove-configure-ac.patch"
SRC_URI[sha256sum] = "5f34ef3a5e1b8666ae77650917d0ec6eb4d7a437b3b66ea667a61158c8f4e8c4"

DEPENDS += "virtual/kernel"

# Since the sources are not a regular kernel module module.bbclass cannot
# be inherited, instead we use module.bbclass as a template.
inherit module-base kernel-module-split autotools

addtask make_scripts after do_patch before do_compile
do_make_scripts[lockfiles] = "${TMPDIR}/kernel-scripts.lock"
do_make_scripts[deptask] = "do_populate_sysroot"

EXTRA_OECONF = "--disable-8139too"

do_compile() {
	unset CFLAGS CPPFLAGS CXXFLAGS LDFLAGS
	oe_runmake KERNEL_PATH=${STAGING_KERNEL_DIR}   \
		   KERNEL_SRC=${STAGING_KERNEL_DIR}    \
		   KERNEL_VERSION=${KERNEL_VERSION}    \
		   CC="${KERNEL_CC}" LD="${KERNEL_LD}" \
		   AR="${KERNEL_AR}"                   \
		   -C ${STAGING_KERNEL_DIR}            \
		   M="${S}"                            \
		   modules
}

do_install() {
	unset CFLAGS CPPFLAGS CXXFLAGS LDFLAGS
	oe_runmake DEPMOD=echo INSTALL_MOD_PATH="${D}" \
	           KERNEL_SRC=${STAGING_KERNEL_DIR}    \
	           CC="${KERNEL_CC}" LD="${KERNEL_LD}" \
		   -C ${STAGING_KERNEL_DIR}            \
		   M="${S}"                            \
	           modules_install
        install -d 755 ${D}${sysconfdir}/udev/rules.d
        echo KERNEL==\"EtherCAT[0-9]*\", MODE=\"0664\" > ${D}${sysconfdir}/udev/rules.d/99-EtherCAT.rules
}

# add all splitted modules to PN RDEPENDS, PN can be empty now
KERNEL_MODULES_META_PACKAGE = "${PN}"
FILES_${PN} = "${sysconfdir}"
# ALLOW_EMPTY_${PN} = "1"
