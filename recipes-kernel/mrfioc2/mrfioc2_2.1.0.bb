SUMMARY = "UIO module for MRF"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS += "virtual/kernel"

# Since the sources are not a regular kernel module module.bbclass cannot
# be inherited, instead we use module.bbclass as a template.
inherit module-base kernel-module-split

addtask make_scripts after do_patch before do_compile
do_make_scripts[lockfiles] = "${TMPDIR}/kernel-scripts.lock"
do_make_scripts[deptask] = "do_populate_sysroot"

do_compile() {
	unset CFLAGS CPPFLAGS CXXFLAGS LDFLAGS
	oe_runmake KERNEL_PATH=${STAGING_KERNEL_DIR}   \
		   KERNEL_SRC=${STAGING_KERNEL_DIR}    \
		   KERNEL_VERSION=${KERNEL_VERSION}    \
		   CC="${KERNEL_CC}" LD="${KERNEL_LD}" \
		   AR="${KERNEL_AR}"                   \
		   -C ${STAGING_KERNEL_DIR}            \
		   M="${S}/mrmShared/linux"            \
		   modules
}

do_install() {
	unset CFLAGS CPPFLAGS CXXFLAGS LDFLAGS
	oe_runmake DEPMOD=echo INSTALL_MOD_PATH="${D}" \
	           KERNEL_SRC=${STAGING_KERNEL_DIR}    \
	           CC="${KERNEL_CC}" LD="${KERNEL_LD}" \
		   -C ${STAGING_KERNEL_DIR}            \
		   M="${S}/mrmShared/linux"            \
	           modules_install
        install -d 755 ${D}${sysconfdir}/udev/rules.d
cat << EOF > ${D}${sysconfdir}/udev/rules.d/99-mrfioc2.rules
KERNEL=="uio*", ATTR{name}=="mrf-pci", GROUP="ioc", MODE="0660"
EOF
}


#PR = "r0"
#PV = "0.1"

SRC_URI = "git://git@bitbucket.org/europeanspallationsource/m-epics-mrfioc2.git;protocol=ssh;tag=v${PV};nobranch=1"
#SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

# add all splitted modules to PN RDEPENDS, PN can be empty now
KERNEL_MODULES_META_PACKAGE = "${PN}"
FILES_${PN} = "${sysconfdir}"
# ALLOW_EMPTY_${PN} = "1"
