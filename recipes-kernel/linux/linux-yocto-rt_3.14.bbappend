COMPATIBLE_MACHINE_append = "|ifc1210"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://0001-add-ifc1210-dts.patch"
SRC_URI += "file://defconfig"

SRCREV_machine = "3428de71031ede23682dd0842b9cfc23ae465f39"
#SRCREV_meta = "${AUTOREV}"
