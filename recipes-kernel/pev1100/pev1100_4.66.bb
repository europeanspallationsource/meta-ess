SUMMARY = "IOxOS IFC1210 kernel modules, user library and tool"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS += "virtual/kernel"

# Since the sources are not a regular kernel module module.bbclass cannot
# be inherited, instead we use module.bbclass as a template.
inherit module-base kernel-module-split

addtask make_scripts after do_patch before do_compile
do_make_scripts[lockfiles] = "${TMPDIR}/kernel-scripts.lock"
do_make_scripts[deptask] = "do_populate_sysroot"

# Build the following
#   PEV1100 module
#   hotplug module
#   libraries
#   XprsMon

do_compile() {
        unset CFLAGS CPPFLAGS CXXFLAGS LDFLAGS

        cp ${S}/drivers/Makefile.ppc ${S}/drivers/Makefile
        oe_runmake KERNEL_PATH=${STAGING_KERNEL_DIR}   \
                   KERNEL_SRC=${STAGING_KERNEL_DIR}    \
                   KERNEL_VERSION=${KERNEL_VERSION}    \
                   CC="${KERNEL_CC}" LD="${KERNEL_LD}" \
                   AR="${KERNEL_AR}"                   \
                   -C ${STAGING_KERNEL_DIR}            \
                   M="${S}/drivers"                    \
                   modules

        oe_runmake KERNEL_PATH=${STAGING_KERNEL_DIR}   \
                   KERNEL_SRC=${STAGING_KERNEL_DIR}    \
                   KERNEL_VERSION=${KERNEL_VERSION}    \
                   CC="${KERNEL_CC}" LD="${KERNEL_LD}" \
                   AR="${KERNEL_AR}"                   \
                   -C ${STAGING_KERNEL_DIR}            \
                   M="${S}/hotplug"                    \
                   modules

        make -C ${S}/lib -f Makefile.ppc  \
        CC="${CC}" CP="${CXX}" LD="${LD}" \
        AR="${AR}" STRIP="${STRIP}"       \
        RANLIB="${RANLIB}" all

        make -C ${S}/src/XprsMon -f Makefile.ppc \
        CC="${CC}" CP="${CXX}" LD="${LD}"        \
        AR="${AR}" STRIP="${STRIP}"              \
        RANLIB="${RANLIB}" OLIB="" XprsMon
}

do_install() {
        unset CFLAGS CPPFLAGS CXXFLAGS LDFLAGS
        oe_runmake DEPMOD=echo INSTALL_MOD_PATH="${D}" \
                   KERNEL_SRC=${STAGING_KERNEL_DIR}    \
                   CC="${KERNEL_CC}" LD="${KERNEL_LD}" \
                   -C ${STAGING_KERNEL_DIR}            \
                   M="${S}/drivers"                    \
                   modules_install

        install -d ${D}${sbindir}
        install ${S}/modules/load ${D}${sbindir}/ioxos_load
        install ${S}/modules/connect ${D}${sbindir}/ioxos_connect
        install ${S}/modules/disconnect  ${D}${sbindir}/ioxos_disconnect

        oe_runmake DEPMOD=echo INSTALL_MOD_PATH="${D}" \
                   KERNEL_SRC=${STAGING_KERNEL_DIR}    \
                   CC="${KERNEL_CC}" LD="${KERNEL_LD}" \
                   -C ${STAGING_KERNEL_DIR}            \
                   M="${S}/hotplug"                    \
                   modules_install

        install -d ${D}${libdir}
        install ${S}/lib/libpev.a ${D}${libdir}/
        install ${S}/lib/libpev++.a ${D}${libdir}/
        install -d ${D}${includedir}
        install ${S}/include/pev*.h ${D}${includedir}/

        install -d ${D}${bindir}
        install -m 0755 ${S}/src/XprsMon/XprsMon ${D}${bindir}/
}

FILES_${PN}   += "${sbindir}/"


SRC_URI = "git://git@bitbucket.org/europeanspallationsource/m-kmod-pev.git;protocol=ssh;tag=v${PV};nobranch=1"
#SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

# add all splitted modules to PN RDEPENDS, PN can be empty now
KERNEL_MODULES_META_PACKAGE = "${PN}"
# FILES_${PN} = ""
# ALLOW_EMPTY_${PN} = "1"
