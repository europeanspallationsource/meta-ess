SUMMARY = "EPICS Environment etc. files"
SECTION = "base"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"
#PR = "r0"

SRC_URI = "git://bitbucket.org/europeanspallationsource/m-epics-environment.git;protocol=https;tag=v${PV};nobranch=1"
#SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"


CONFFILES_${PN} += "${sysconfdir}/profile.d/ess_epics_env.sh"

# TODO: Improve compilation instruction by supplying the host-arch based on os/arch
do_compile () {
        (cd ${S}/meta; python gen_profile_d.py --epics-host-arch eldk56-e500v2)
}

do_install () {
	install -d ${D}${sysconfdir}/profile.d
	install -m 0644 ${S}/meta/ess_epics_env.sh ${D}${sysconfdir}/profile.d

	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${S}/meta/systemd/system/ioc@.service ${D}${systemd_unitdir}/system
	install -m 0644 ${S}/meta/systemd/system/ioc-master.service ${D}${systemd_unitdir}/system
	install -m 0644 ${S}/meta/systemd/system/ioc-master.path ${D}${systemd_unitdir}/system
	install -m 0644 ${S}/meta/systemd/system/ess-boot.service ${D}${systemd_unitdir}/system

	install -d ${D}${bindir}
	install -m 0755 ${S}/scripts/ess_boot.py ${D}${bindir}

	install -d ${D}${sysconfdir}/systemd/system/multi-user.target.wants
        ln -s ${systemd_unitdir}/system/ioc-master.path ${D}${sysconfdir}/systemd/system/multi-user.target.wants
        ln -s ${systemd_unitdir}/system/ess-boot.service ${D}${sysconfdir}/systemd/system/multi-user.target.wants
}

FILES_${PN} += "${systemd_unitdir}"

inherit useradd

USERADD_PACKAGES = "${PN}"

USERADD_PARAM_${PN} = "--system ioc"

# Prevents do_package failures with:
# debugsources.list: No such file or directory:
# INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
