FILESEXTRAPATHS_prepend := "${THISDIR}/${BPN}:"

SRC_URI += "file://dhcp.network"

hostname = ""
dirs755 += "/opt/epics /opt/startup"

do_install_append () {
        install -d ${D}${sysconfdir}/systemd/network
        install -m 0644 ${WORKDIR}/dhcp.network ${D}${sysconfdir}/systemd/network
}
