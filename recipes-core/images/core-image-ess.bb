DESCRIPTION = "Extension of core-image-minimal with ESS specific features."

IMAGE_FEATURES += "ssh-server-openssh dbg-pkgs dev-pkgs tools-debug read-only-rootfs"

#LICENSE = "MIT"

# Dependencies on kernel modules are listed in the machine configuration.

# xprsmon is ioxos tool for accessing bits on the IFC1210
# pevlib-dev are header files for pevlib
# pev-mod will install scripts for kernel-module-pev
# python-argparse is missing in python-modules

IMAGE_INSTALL += "\
    packagegroup-core-boot \
    packagegroup-core-full-cmdline \
    python-modules \
    python-argparse \
    ${CORE_IMAGE_EXTRA_INSTALL} \
    inetutils \
    nfs-utils-client \
    boost \
    curl \
    libxml2 \
    libpcre \
    re2c \
    pev1100 \
    mrfioc2 \
    procserv \
    bash-completion \
    epics-env \
    nss-pam-ldapd \
    u-boot-fw-utils \
    htop \
    vim \
    valgrind \
    "

inherit core-image
