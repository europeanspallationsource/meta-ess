FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
SRC_URI_append = " file://0001-remove-privatetmp.patch"

PACKAGECONFIG = "xz resolved networkd"

USERADD_PARAM_${PN} += ";--system systemd-network;--system systemd-resolve"
