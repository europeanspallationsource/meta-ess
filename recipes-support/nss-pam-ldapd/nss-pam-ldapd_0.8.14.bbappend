SRC_URI += "file://ess-ldap.patch \
            file://ldap.pam \
"

CONFFILES_${PN} += "${sysconfdir}/pam.d/ldap"

do_install_append() {
        install -D -m 0644 ${WORKDIR}/ldap.pam ${D}${sysconfdir}/pam.d/ldap
}
