# Remove patch that depends on cyrus sasl
SRC_URI = "\
    ftp://ftp.openldap.org/pub/OpenLDAP/openldap-release/${P}.tgz \
    file://man-slapd.patch \
    file://evolution-ntlm.patch \
    file://slapi-errorlog-file.patch \
    file://ldapi-socket-place.patch \
    file://wrong-database-location.patch \
    file://index-files-created-as-root.patch \
    file://libldap-symbol-versions.patch \
    file://getaddrinfo-is-threadsafe.patch \
    file://do-not-second-guess-sonames.patch \
    file://contrib-modules-use-dpkg-buildflags.patch \
    file://smbk5pwd-makefile.patch \
    file://autogroup-makefile.patch \
    file://ldap-conf-tls-cacertdir.patch \
    file://add-tlscacert-option-to-ldap-conf.patch \
    file://fix-ftbfs-binutils-gold.patch \
    file://fix-build-top-mk.patch \
    file://no-AM_INIT_AUTOMAKE.patch \
    file://switch-to-lt_dlopenadvise-to-get-RTLD_GLOBAL-set.diff.patch \
    file://no-bdb-ABI-second-guessing.patch \
    file://heimdal-fix.patch \
    file://slapd-remove-empty-run-dir.patch \
    file://install-strip.patch \
"

# Remove dependency on cyrus-sasl

PACKAGECONFIG = "openssl modules \
                 ldap meta monitor null passwd shell proxycache dnssrv \
                 bdb hdb mdb \
                 "
