SUMMARY = "procServ"
SECTION = "base"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-3.0;md5=c79ff39f19dfec6d293b95dea7b07891"
PR = "r0"

SRC_URI = "git://github.com/ralphlange/procServ.git"
SRCREV = "4bff898ac25ca596098eb84934c70a3835be1058"

S = "${WORKDIR}/git"

EXTRA_OECONF = "--disable-doc"


inherit autotools
